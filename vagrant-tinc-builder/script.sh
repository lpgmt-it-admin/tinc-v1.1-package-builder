#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

cd /home/vagrant

echo "Updating OS"
sudo apt-get update
sudo apt-get upgrade -y

echo "Installing general compilation dependencies, package bulding dependencies and specific tinc build dependencies"
sudo apt-get install -y build-essential fakeroot devscripts
sudo apt-get install -y libssl-dev debhelper texinfo zlib1g-dev liblzo2-dev libncurses5-dev libreadline-dev libvdeplug-dev

echo "Expanding tinc source package into working area"
mkdir -p src/debian
cd src/debian
cp /vagrant/tinc* ./
dpkg-source -x tinc_1.1~pre11-1.dsc
cd tinc-1.1~pre11/
debuild -b -uc -us
cp ../*.deb /home/vagrant/guest-output

echo "Done!"
echo "On the host, execute:"
echo "   vagrant rsync-pull"
echo "     ...to sync the created packages to ../guest-output on the host"