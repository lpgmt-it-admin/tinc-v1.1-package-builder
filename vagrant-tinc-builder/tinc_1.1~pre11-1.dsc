-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: tinc
Binary: tinc, tinc-gui
Architecture: any all
Version: 1.1~pre11-1
Maintainer: Guus Sliepen <guus@debian.org>
Homepage: http://www.tinc-vpn.org/
Standards-Version: 3.9.6
Build-Depends: libssl-dev (>> 1.0.0), debhelper (>= 9), texinfo, zlib1g-dev, liblzo2-dev, libncurses5-dev, libreadline-dev, libvdeplug-dev
Package-List:
 tinc deb net optional arch=any
 tinc-gui deb net optional arch=all
Checksums-Sha1:
 ee6920d87b377c39da71d7ad5807a37f55e4e3be 659259 tinc_1.1~pre11.orig.tar.gz
 8dc3ed618ba1168abcf58f2dd35d72aec9587322 11100 tinc_1.1~pre11-1.debian.tar.xz
Checksums-Sha256:
 942594563d3aef926a2d04e9ece90c16daf1c700e99e3b91ff749e8377fbf757 659259 tinc_1.1~pre11.orig.tar.gz
 2abd31441af79d6d676e6cf173b3eb780bb8d2378d747961d5403a7351870263 11100 tinc_1.1~pre11-1.debian.tar.xz
Files:
 e9cc2971927d31e703f0c2b1791a4edf 659259 tinc_1.1~pre11.orig.tar.gz
 48bff2c8e8af93b11a40cbb9d2fb45f4 11100 tinc_1.1~pre11-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBCAAGBQJUro0FAAoJED9JDeuHHvn6i7gP/2tn9F+7/KVhLe8umJy9c3ta
IwS7DOwNqXJkLyB4Yts5n9rhwRsdRiios3x0fwTTOnI2IUoHd/aX3wfQeg/sn79N
xkqcbK2H+rAhSxoisu5UrqCZ3mChB+GoNTnCxbKaGrt1r4GD73Le4fzrBdWJuruX
Sip+VsJ1JjVEhSeU6bQDh9Z6Fsi5vqgH3OTbYe2sFF0ghuHSa5LjJAhmbH8zs5QX
PEREzNMlQuoWIyGDvEMlgsDWuzhlOPM0diUBnND5OZsOfRd28iYQCs4te4BtYnM5
Ozbyni1sXeEBXA00N7eenG+mnTwp5eo1BtLylgjyx+TrkzKsTiY/2Iyb/d+tbWOT
MvrzQ9jJcZWEXJ17haTD2BFBgmeX6rf32vhQpBbygH/9TbqaiV0vjbdg71JnzP1M
359y/wCS3b7JbJq2Gx98UuZWxKsboD8FBETX1IbTRTmAuyFvRMBClAX5jYsWy2Qh
9E6WjjQY/tMauBKDJ4qVdQv0BDe1gX9hRnH9UXJFaXSzKG1rMTCY4izIMfgQ6ynw
md5pRfDlH8mwLnGKjMfuIEztqlfbKCnqrBY3jDJH20fahRTEdapXqlJ85djlDNA2
7SoF43uDHr5ZSpoHGovvb1jU5jecll78nkMI9Hb0j/V1sfjsGdwVSricJg5KioCQ
ui5JqD6+h5pXzqRMn7Qd
=Owra
-----END PGP SIGNATURE-----
